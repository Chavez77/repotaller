var clientesObtenidos;
var rutaBandera ="https://www.countries-ofthe-world.com/flags-normal/flag-of-";

function getClientes() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //console.table(JSON.parse(request.responseText).value)
            clientesObtenidos = request.responseText;
            procesarClientes()
        }
    }
    request.open("GET", url, true);
    request.send();
}

function procesarClientes() {
    var JSONClientes = JSON.parse(clientesObtenidos);
    var divTabla = document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tabBody = document.createElement("tbody");

    tabla.classList.add("tabla");
    tabla.classList.add("table-hover");
    for(var i = 0; i < JSONClientes.value.length; i++){
        console.log(JSONClientes.value[i].ProductName);
        var newFila = document.createElement("tr");
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONClientes.value[i].ContactName;
        var columnaCiudad = document.createElement("td");
        columnaCiudad.innerText = JSONClientes.value[i].City;
        var columnaPais = document.createElement("td");
        var imgBandera = document.createElement("img");

        var pais = JSONClientes.value[i].Country;

        if(pais == "UK"){
            imgBandera.src = rutaBandera + "United-Kingdom.png";

        }else{
            imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";

        }
        imgBandera.classList.add("flag");

        columnaPais.appendChild(imgBandera);

        //columnaPais.innerText = JSONProductos.value[i].Country;


        newFila.appendChild(columnaNombre);
        newFila.appendChild(columnaCiudad);
        newFila.appendChild(columnaPais);

        tabBody.appendChild(newFila);
    }

    tabla.appendChild(tabBody);
    divTabla.appendChild(tabla);
}
