var productosObtenidos;

function getProductos() {
    var url = 'https://services.odata.org/V4/Northwind/Northwind.svc/Products';
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //console.table(JSON.parse(request.responseText).value);
            productosObtenidos = request.responseText;
            procesarProductos();

        }
    }
    request.open("GET", url, true);
    request.send();
}

function procesarProductos() {
    var divTabla = document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tabBody = document.createElement("tbody");
    JSONProductos = JSON.parse(productosObtenidos);

    tabla.classList.add("tabla");
    tabla.classList.add("table-hover");
    for(var i = 0; i < JSONProductos.value.length; i++){
        var newFila = document.createElement("tr");

        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONProductos.value[i].ProductName;
        var columnaPrecio = document.createElement("td");
        columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
        var columnaStock = document.createElement("td");
        columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

        newFila.appendChild(columnaNombre);
        newFila.appendChild(columnaPrecio);
        newFila.appendChild(columnaStock);

        tabBody.appendChild(newFila);
    }

    tabla.appendChild(tabBody);
    divTabla.appendChild(tabla);
}
